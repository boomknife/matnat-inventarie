INSERT INTO `user` 
( `username`
, `full_name`
, `email`
, `privilege`
, `hash_method`
, `salt`
, `hash`)
WITH RECURSIVE `salt` (`salt`) AS (SELECT left(md5(rand()), 16))
SELECT 
  'hugo'
, 'Hugo Hörnquist'
, 'hugo@lysator.liu.se'
, user_types.id
, hash_method.id
, salt.salt
, md5(concat(salt.salt, 'password'))
FROM salt, hash_method, user_types
WHERE hash_method.name = 'md5'
  AND user_types.title = 'admin';
