<!doctype html><?php
/*
 * Page for administrating users.
 *
 * This page is only accessible for admins.
 * From here, the level of all users should be modifyable,
 * new users may be added, and old users removed.
 */
?><html>
<head>
	<meta charset="UTF-8"/>
<?php
require 'lib/init.php';
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>MatNats lagersystem</title>
	<link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>
<h1>MatNats Lagersystem</h1>

<?php if (array_key_exists("error", $_SESSION)) { ?>
<div class="errmsg">
<?php echo "${_SESSION["error"]}"; 
/* Error is now printed. Remove it so next reload doesn't show it */
unset($_SESSION["error"]);
?>
</div>
<?php } ?>

<h2>Användare</h2>
<form>
<table>
	<thead>
		<tr>
			<th></th>
			<th>Användarnamn</th>
			<th>Namn</th>
			<th>Epost</th>
			<th>Privilegie</th>
		</tr>
	</thead>
	<tbody>
<?php
global $mysqli;
$stmt = $mysqli->prepare("
SELECT user.id
	 , username
	 , full_name
	 , email
	 , privilege AS type_id
     , user_types.title as type_name
FROM user
LEFT JOIN user_types ON user_types.id = user.privilege
");

$stmt->execute();
$stmt->bind_result($id, $username, $full_name, $email, $type_id, $type_name);
while ($stmt->fetch()) { ?>
<tr>
	<td><input type="checkbox" /></td>
	<td><?php echo $username; ?></td>
	<td><input value="<?php echo $full_name; ?>" name="<?php echo $username; ?>-name" /></td>
	<td><input value="<?php echo $email; ?>" name="<?php echo $username; ?>-email" type="email"/></td>
	<td>
		<select name="privilege">
		</select>
</td>
</tr>
<?php } ?>
	</tbody>
	<thead>
	</thead>
		<tr>
			<th></th>
			<th>Användarnamn</th>
			<th>Namn</th>
			<th>Epost</th>
			<th>Privilegie</th>
		</tr>
	</thead>
</table>
<input type="submit" value="Spara ändringar"/>
</form>

<h2>Skapa ny användare</h2>
<form class="basic-form" action="/post/create-user.php" method="POST">
    <label for="username">Användarnamn:</label>
    <input id="username" name="username" required="required" placeholder="anders"/>

    <label for="name">Fullständigt namn</label>
    <input id="name" name="name" required="required" placeholder="Anders Andersson"/>


    <label for="password">Lösenord:</label>
    <input id="password" name="password" required="required" placeholder="Lösenord..." type="password" />

    <label for="email">Epostaddress:</label>
    <input id="email" name="email" type="email" required="required" placeholder="Epostaddress (privat)..."/>

    <label for="privilege">Privilegie</label>
    <select name="privilege" id="privilege">
    <?php
$result = $mysqli->query("SELECT title FROM user_types");
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
?>
    <option ><?php echo $row['title']; ?>
</option>
<?php
}
    ?>
    </select>

    <input type="submit" value="Skapa användare" />
<?php

?>
    </select>
</form>

</body>
</html>
