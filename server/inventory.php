<!doctype html><?php
/*
 * Primary page of the application.
 * Contains the table of inventory, which will be a form if you have
 * the rights to check in and out items.
 */
?><html>
<head>
	<meta charset="UTF-8"/>
<?php 
require 'lib/init.php';
if (! ($user = validate_user())) {
    die();
}
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title></title>
	<link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>

<!-- TODO validate that user is logged in -->
<?php if (array_key_exists("error", $_SESSION)) { ?>
<div class="errmsg">
<?php echo "${_SESSION["error"]}"; 
/* Error is now printed. Remove it so next reload doesn't show it */
unset($_SESSION["error"]);
?>
</div>
<?php } ?>

<form action="/post/modify_inventory.php" method="POST">
<table>
    <thead>
        <tr>
            <th>Förråd</th>
            <th>Vara</th>
            <th>Lager</th>
            <th>Enhet</th>
<?php if ($user['privilige'] === 'admin' || $user['privilige'] === 'editor') { ?>
            <th>Ingående</th>
            <th>Utgående</th>
<?php } ?>
        </tr>
    </thead>
    <tbody>
<?php 
// $stmt->bind_param 

$stmt1 = $mysqli->prepare("
SELECT id, title, stock, storage, unit
FROM products
");

$stmt1->execute();
$stmt1->bind_result($id, $title, $stock, $storage_id, $unit);
while ($stmt1->fetch()) { 
	// TODO Stop using a second database connection
    $stmt2 = $mysqli1->prepare("SELECT title FROM storages WHERE id = ?");
    $stmt2->bind_param("s",$storage_id);
    $stmt2->execute();
    $stmt2->bind_result($storage);
    $stmt2->fetch();
    $stmt2->free_result();
?>
<tr>
    <td><?php echo $storage;?></td>
    <td align="right"><?php echo $title; ?></td>
    <td align="right"><?php echo $stock; ?></td>
    <td><?php echo $unit; ?></td>
<?php if ($user['privilige'] === 'admin' || $user['privilige'] === 'editor') { ?>
    <td><input type="number" step="0.01" name="in_<?php echo $id?>"/></td>
    <td><input type="number" step="0.01" name="out_<?php echo $id?>" /></td>
<?php } ?>
</tr>
<?php } ?>
    </tbody>
    <thead>
        <tr>
            <th>Förråd</th>
            <th>Vara</th>
            <th>Lager</th>
            <th>Enhet</th>
<?php if ($user['privilige'] === 'admin' || $user['privilige'] === 'editor') { ?>
            <th>Ingående</th>
            <th>Utgående</th>
<?php } ?>
        </tr>
    </thead>
</table>
<input type="submit" value="Begär uttag" />
</form>

</body>
</html>
