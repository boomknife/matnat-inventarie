<!doctype html><?php
?><html>
<head>
	<meta charset="UTF-8"/>
<?php 
require 'lib/init.php';
require 'lib/header_and_footer.php';
// TODO Also check that the user has sufficient permissions
if (! ($user = validate_user())) {
    die();
}
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title></title>
	<link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>
<?php if (array_key_exists("error", $_SESSION)) { ?>
<div class="errmsg">
<?php echo "${_SESSION["error"]}"; 
/* Error is now printed. Remove it so next reload doesn't show it */
unset($_SESSION["error"]);
?>
</div>
<?php } ?>

<h2>Existerande lager</h2>
<form>
    <table>
        <thead>
            <tr>
                <th>Produkt</th>
                <th>Lager</th>
            </tr>
        </thead>
        <tbody>
<tr>
    <td><?php echo $product; ?></td>
    <td><input name="<?php echo $product ?>-count" type="number" value="<?php echo $product; ?>" /></td>
</tr>
        </tbody>
        <thead>
            <tr>
                <th>Produkt</th>
                <th>Lager</th>
            </tr>
        </thead>
    </table>
    <input type="submit" value="Spara ändringar"/>
</form>

<h2>Ny produkt</h2>
<form class="basic-form" action="/post/create-product.php" method="POST">
    <label for="name">Produktnamn</label>
    <input name="name" required="required" placeholder="Green Party Hats"/>

    <label for="stock">Ingående lager</label>
    <input type="number" name="stock" required="required" placeholder="100"/>

    <label for="unit">Enhet</label>
    <input name="unit"required="required" placeholder="styck">

    <label for="storage">Förråd</label>
    <select name="storage" id="storage">
    <?php
    $result = $mysqli->query("SELECT title FROM storages");
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    ?>
    <option ><?php echo $row['title']; ?>
    </option>
    <?php
    }
    ?>
    </select>


    <input type="submit" value="Skapa produkt"/>
</form>

<h2>Historik av utlåningar</h2>
<ul>
<?php
$stmt = $mysqli->query("
SELECT user.id
     , user.full_name
     , product.title
     , inventory_log.amount
     , inventory_log.`when`
    FROM inventory_log
LEFT JOIN user ON inventory_log.user = user.id
LEFT JOIN products ON products.id = inventory_log.item
");

$stmt->execute();
$stmt->bind_result($id, $name, $title, $amount, $when);
while ($stmt->fetch()) { ?>
    <li>Den <?php echo $when; ?>
<?php
    if ($amount > 0) {
        echo "hämtade";
    } else {
        echo "lämnade";
    }
?>
<?php echo $name ?>
<?php echo abs($amount); ?> av
<?php echo $title; ?>
</li>
<?php  } ?>
</ul>

</body>
</html>
