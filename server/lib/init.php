<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(E_ALL);
error_reporting(~E_STRICT);

$ini_array = parse_ini_file("../matnat.ini", true);


// $COOKIE_MATNAT_USER = "matnat_user";
// TODO validate configuration
$COOKIE_MATNAT_SESSION = $ini_array["MatNat"]["cookie"];

session_start();

// TODO validalite configuration
$mysqli = new mysqli($ini_array["Database"]["server"], $ini_array["Database"]["user"], $ini_array["Database"]["password"], $ini_array["Database"]["database"]);
// TODO remove this

$mysqli1 = new mysqli($ini_array["Database"]["server"], $ini_array["Database"]["user"], $ini_array["Database"]["password"], $ini_array["Database"]["database"]);
// TODO Only initialize database when needed. This is needlessly slow
// $handle = fopen("schema.sql", "r");
// $sql_data = fread($handle, filesize("schema.sql"));
// 
// $mysqli->multi_query($sql_data);


/**
 * Checks if the user is logged in.
 * 
 * Either returns 'false', or a record containing information about
 * the user, if it's logged in.
 */
function validate_user() {
    global $COOKIE_MATNAT_SESSION;
    global $mysqli;

    if (! array_key_exists($COOKIE_MATNAT_SESSION, $_COOKIE)) {
        error_log("No cookie for user");
        return false;
    }


    $stmt = $mysqli->prepare("
SELECT username, full_name, email, user_types.title
FROM session
LEFT JOIN user ON user.id = session.user
LEFT JOIN user_types ON user_types.id = user.privilege
WHERE session.expires > CURRENT_TIMESTAMP
  AND session_cookie = ?
");

    // error_log("Using cookie ${_COOKIE[$COOKIE_MATNAT_SESSION]}");
	$stmt->bind_param("s", $_COOKIE[$COOKIE_MATNAT_SESSION]);

    $stmt->execute();

    $stmt->bind_result($username, $name, $email, $privilige);

    if (! $stmt->fetch()) {
        // error_log("User not authenticated");
        return false;
    }

    // error_log("User ${username} IS authenticated");

    return array (
        'username'  => $username,
        'name'      => $name,
        'email'     => $email,
        'privilige' => $privilige,
    );
}

/**
 * Attempts to login a user.
 *
 * On failure false is returned, while on success true is returned.
 */
function login_user($username, $password) {
    global $COOKIE_MATNAT_SESSION;
    global $mysqli;

	$mysqli->begin_transaction();

    $stmt = $mysqli->prepare("
SELECT user.id, hash_method.name, salt, hash
FROM user
LEFT JOIN hash_method ON hash_method.id = hash_method
WHERE username = ?
");

    $stmt->bind_param("s", $username);
    $stmt->execute();

    $stmt->bind_result($user_id, $hashmethod, $salt, $refhash);

    if (! $stmt->fetch()) {
        /* Incorrect username */
        // $_SESSION["error"] = "Felaktigt lösenord eller användarnamn.";
        $_SESSION["error"] = "Okänt användarnamn";
		$mysqli->rollback();
        return false;
    }

    $stmt->free_result();
	$stmt->close();

    switch ($hashmethod) {
    case 'md5':
        $hash = md5($salt . $password);
        break;
    case 'sha256':
        $hash = hash("sha256", $salt . $password);
        break;
    default:
        // TODO possibly better trace
        error_log("Unknown hash method: $hashmethod");
        $_SESSION["error"] = "Udda pyttipanna ($hashmethod). Kontakta driftsanvsarig";
        return false;
    }

    if ($hash !== $refhash) {
        /* Incorrect username */
        // $_SESSION["error"] = "Felaktigt lösenord eller användarnamn.";
        $_SESSION["error"] = "Felaktigt lösenord";
        return false;
    }


    $stmt = $mysqli->prepare("
INSERT INTO session (session_cookie, expires, user)
WITH end_time (end) AS (SELECT date_add(CURRENT_TIMESTAMP, INTERVAL 1 DAY))
SELECT uuid(), end_time.end, ? FROM end_time
ON DUPLICATE KEY UPDATE expires = end_time.end
RETURNING session_cookie, unix_timestamp(expires)
");

    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $stmt->bind_result($cookie, $expires);

    if (! $stmt->fetch()) {
        // TODO HTTP 500 series
		$mysqli->rollback();
        die();
        return false;
    }

	$stmt->close();

	$mysqli->commit();

    error_log("cookie = $cookie");

    setcookie(
        $COOKIE_MATNAT_SESSION,
        $cookie,
        $expires,
        "/",
    );

    error_log("Cookie sat");

	return true;
}

function create_user($username, $password, $email, $name, $privilege){
    global $mysqli;

	$mysqli->begin_transaction();

    $stmt = $mysqli->prepare("
SELECT title from user_types
");
    $arr = array();
    $stmt->execute();
    $stmt->bind_result($privilege_type);
    while($stmt->fetch()){
        $arr[] = $privilege_type;
    }
    if(empty($username)||empty($password)||empty($email)||empty($name)||empty($privilege)){
        $_SESSION["error"] = "Tomt eller saknat fält";
        return false;
    }
    if(!in_array($privilege,$arr)){
        $_SESSION["error"] = "Ogiltigt privilegie: $privilege";
        return false;
    }
    $stmt = $mysqli->prepare("
select id from hash_method where name='sha256'
");
    $stmt->execute();
    $stmt->bind_result($hash_id);
    if(!$stmt->fetch()){
        $_SESSION["error"] = "fel hash_id";
        return false;
    }
    $sha256_id = $hash_id;
    $stmt->free_result();
    $stmt = $mysqli->prepare("
select id from user_types where title=?
");
    $stmt->bind_param("s",$privilege);
    $stmt->execute();
    $stmt->bind_result($privilege_type);
    if(!$stmt->fetch()){
        $_SESSION["error"] = "fel priv._id";
        return false;
    }
    $privilege_id = $privilege_type;
    $salt = substr(md5(rand()),0,16);
    $stmt->free_result();
    $hash = hash("sha256",$salt . $password);
    $stmt = $mysqli->prepare("
insert into user (username, full_name, email, privilege, hash_method, salt, hash) values
(?,?,?,?,?,?,?)    
");
    $stmt->bind_param("sssssss",$username, $name, $email, $privilege_id, $sha256_id, $salt, $hash);
    $stmt->execute();

	$mysqli->commit();

    return true;
}

function modify_product($id,$in,$out){
    global $mysqli;
    $stmt = $mysqli->prepare("select stock from products where id = ?");
    $stmt->bind_param("s",$id);
    $stmt->execute();
    $stmt->bind_result($stock);
    $stmt->fetch();
    if(!empty($in)){
        $stock=$stock+$in;
    }
    if(!empty($out)){
        $stock=$stock-$out;
    }
    $stmt->free_result();
    if($stock<0){
        $_SESSION["error"] = "kan inte ta ut mer än vad som finns";
        return false;
    }
    $stmt = $mysqli->prepare("update products set stock = ? where id = ?");
    $stmt->bind_param("ss",$stock,$id);
    $stmt->execute();
    $stmt->free_result();
    return true;
}

function create_product($stock,$name,$unit,$storage){
    global $mysqli;
    $stmt = $mysqli->prepare("
select id from storages where title = ?
");
    $stmt->bind_param("s",$storage);
    $stmt->execute();
    $stmt->bind_result($storage_id);
    if(!$stmt->fetch()){
        $_SESSION["error"] = "ogiltigt förrådsid";
        return false;
    }
    $storage_id1=$storage_id;
    $stmt->free_result();
    $stmt = $mysqli->prepare("insert into products (title, stock,unit,storage) values (?,?,?,?)");
    $stmt->bind_param("ssss",$name,$stock,$unit,$storage_id1);
    try {$stmt->execute();}
    catch (Exception $e) {
        $stmt->free_result();
        $_SESSION["error"] = "Denna produkt finns redan i detta förråd";
        return false;
    }
    $stmt->free_result();
    return true;
}

function logout_user(){
    global $COOKIE_MATNAT_SESSION;
    global $mysqli;
    
}
