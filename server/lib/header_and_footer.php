<?php

function page_header($title){?>
    <!doctype html><html>
<head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php echo $title;?></title>
        <link type="text/css" rel="stylesheet" href="style.css"/>
</head>
<body>
<header>
<form class="basic-form" action="/post/logout_user.php" method="POST">
    <input type="submit" value="Logga Ut"/>
</form>
</header>
<?php if (array_key_exists("error", $_SESSION)) { ?>
<div class="errmsg">
<?php echo "{$_SESSION["error"]}"; 
/* Error is now printed. Remove it so next reload doesn't show it */
unset($_SESSION["error"]);
?>
</div>
<?php
}
}
function footer(){?>
<footer id="footer">
    <div>
        Vid problem med sidan, maila <a  href="mailto:boomknife@lysator.liu.se">boomknife@lysator.liu.se</a>
    </div>
</footer>

</body>
</html>


<?php
}
