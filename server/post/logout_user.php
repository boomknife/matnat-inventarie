<?php

require "../lib/init.php";

logout_user();

http_response_code(303);
header("location: /");

die();
