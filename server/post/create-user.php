<?php

// TODO validate that the method is POST
// TODO check the users logged in status
// TODO check if the current user has the privilege to create users

require "../lib/init.php";

create_user(
    username: $_POST['username'],
    name: $_POST['name'],
    password: $_POST['password'],
    email: $_POST['email'],
    privilege: $_POST['privilege']
);

http_response_code(303);
header("Location: /users.php");
die();
