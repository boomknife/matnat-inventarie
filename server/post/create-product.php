<?php

// TODO validate that the method is POST

require "../lib/init.php";

// TODO check the users logged in status
// TODO check if the current user has the privilege to create users

create_product(
    name: $_POST['name'],
    stock: $_POST['stock'],
    storage: $_POST['storage'],
    unit: $_POST['unit']
);

http_response_code(303);
// TODO indicate that the product is created
header("Location: /manage-inventory.php");

die();
