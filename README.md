### TODO
- Set database engine to InnoDB
- Set character collation to UTF8mb4

#### Deletion of users?
Should it be possible to delete users?
If a user is deleted, it should deffinitely be logged out
But what happens with logs and similar?


### Setup

#### Create database user

{{{mysql
CREATE DATABASE matnat;
CREATE USER 'matnat'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON matnat.* TO 'matnat'@'localhost';
}}}

#### Enabling PHP MySQLi
In `/etc/php/php.ini`, ensure that the line `extension=mysqli` is
present (and uncommented).


--------------------------------------------------

### Övrigt:

#### Byte av användarnamn:
Bör vara trivilat, då varje användare även har ett unikt id

#### Borttagande av användare
Har problamet att historiken går sönder. Bör gå att lösa genom att ta
bort alla inlägg i historiken relaterat till den användaren, samt se
till att användaren i fråga har 0 i saldo vid borttagning.

#### Meny för hantering av tillgängliga prodkuter
Meny vilken låter en administratör lägga till och ta bort produkter
från systemet. Vid borttagning bör en spärr finnas vilken kräver att
nuvarande saldo är 0.

#### Meny för manuell hantering av lagersaldo
Lagersaldo kan för nuvarande enbart hanteras genom utlåningar och
återlämningar. Meny krävs för att manuellt ändra mängden, både för
inköp och för svinn.

### Framtida förbättringar
- Logg för svinn
- Roller är för tillfället "hårdkodade". Potentiellt introducera
  "numeriska" roller, vilket låter alla kontroller i koden vara
  jämförelseoperationer.
- Förfallodatum för bokningar

